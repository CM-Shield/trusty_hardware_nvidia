/*
 * Copyright (c) 2015-2018, NVIDIA CORPORATION. All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <assert.h>
#include <err.h>
#include <debug.h>
#include <trace.h>
#include <rand.h>
#include <string.h>
#include <lib/heap.h>
#include <arch/ops.h>
#include <platform.h>
#include <platform/memmap.h>
#include <platform/plat_smcall.h>
#include <target/debugconfig.h>
#include <platform/boot_params.h>
#include <platform/platform_p.h>
#include <dev/interrupt/arm_gic.h>
#include <dev/timer/arm_generic.h>
#include <lk/init.h>
#include <platform/gic.h>
#include <lib/cpus/denver.h>

#if WITH_KERNEL_VM
#include <kernel/vm.h>
#else
#error "KERNEL_VM is required"
#endif

#include <lib/sm.h>

#define LOCAL_TRACE 0

#define ARM_GENERIC_TIMER_INT_CNTV 27
#define ARM_GENERIC_TIMER_INT_CNTPS 29
#define ARM_GENERIC_TIMER_INT_CNTP 30

#define ARM_GENERIC_TIMER_INT_SELECTED(timer) ARM_GENERIC_TIMER_INT_ ## timer
#define XARM_GENERIC_TIMER_INT_SELECTED(timer) ARM_GENERIC_TIMER_INT_SELECTED(timer)
#define ARM_GENERIC_TIMER_INT XARM_GENERIC_TIMER_INT_SELECTED(TIMER_ARM_GENERIC_SELECTED)

#define NR_SMC_REGS 6

/* MCE command enums for SMC calls */
enum {
	MCE_SMC_ENTER_CSTATE,
	MCE_SMC_UPDATE_CSTATE_INFO,
	MCE_SMC_UPDATE_XOVER_TIME,
	MCE_SMC_READ_CSTATE_STATS,
	MCE_SMC_WRITE_CSTATE_STATS,
	MCE_SMC_IS_SC7_ALLOWED,
	MCE_SMC_ONLINE_CORE,
	MCE_SMC_CC3_CTRL,
	MCE_SMC_ECHO_DATA,
	MCE_SMC_READ_VERSIONS,
	MCE_SMC_ENUM_FEATURES,
	MCE_SMC_ROC_FLUSH_CACHE_TRBITS,
	MCE_SMC_ENUM_READ_MCA,
	MCE_SMC_ENUM_WRITE_MCA,
	MCE_SMC_ROC_FLUSH_CACHE_ONLY,
	MCE_SMC_ROC_CLEAN_CACHE_ONLY,
	MCE_SMC_ENABLE_LATIC,
	MCE_SMC_ENUM_MAX = 0xFF,	/* enums cannot exceed this value */
};
struct mce_regs {
	uint64_t args[NR_SMC_REGS];
};

uint32_t debug_uart_id = DEFAULT_DEBUG_PORT;

extern status_t process_boot_params(void);

/* The following variables might be updated by platform_reset code
 * to adjust amount and location of physical RAM we are alloved to use
 */
uint32_t  _mem_size = MEMSIZE;
uintptr_t _mem_phys_base = MEMBASE + KERNEL_LOAD_OFFSET;

extern void _start(void);
extern ulong lk_boot_args[4];

void platform_early_init(void)
{
	arm_generic_timer_init(ARM_GENERIC_TIMER_INT, 0);

	// The t210 cboot sends the eks size in arg2, but trusty expects the
	// boot param size there. Add the size of the boot params struct, so
	// trusty will allow access to the entire necessary range.
	lk_boot_args[2] += sizeof(boot_params_t);
}

void platform_init(void)
{
	status_t err;

	/* process boot args (cmdline and eks data) */

	if ((err = process_boot_params()) != NO_ERROR) {
		panic("Fatal error: Failed to process boot params\n");
	}

	/* setup debug port passed in boot args */
	platform_init_debug_port(debug_uart_id);
}

static inline uint plat_curr_cpu_num()
{
#ifdef ARCH_ARM64
	return arm64_curr_cpu_num();
#else
	return arm_curr_cpu_num();
#endif
}

/* initial memory mappings. parsed by start.S */
struct mmu_initial_mapping mmu_initial_mappings[] = {
	/* Mark next entry as dynamic as it might be updated
	   by platform_reset code to specify actual size and
	   location of RAM to use */
	{ .phys = MEMBASE + KERNEL_LOAD_OFFSET,
	  .virt = KERNEL_BASE + KERNEL_LOAD_OFFSET,
	  .size = MEMSIZE,
	  .flags = MMU_INITIAL_MAPPING_FLAG_DYNAMIC,
	  .name = "ram" },

	{ .phys = REGISTER_BANK_0_PADDR,
	  .virt = REGISTER_BANK_0_VADDR,
	  .size = REGISTER_BANK_SIZE,
	  .flags = MMU_INITIAL_MAPPING_FLAG_DEVICE,
	  .name = "bank-0" },

	{ .phys = REGISTER_BANK_1_PADDR,
	  .virt = REGISTER_BANK_1_VADDR,
	  .size = REGISTER_BANK_SIZE,
	  .flags = MMU_INITIAL_MAPPING_FLAG_DEVICE,
	  .name = "bank-1" },

	{ .phys = REGISTER_BANK_2_PADDR,
	  .virt = REGISTER_BANK_2_VADDR,
	  .size = REGISTER_BANK_SIZE,
	  .flags = MMU_INITIAL_MAPPING_FLAG_DEVICE,
	  .name = "bank-2" },

	{ .phys = REGISTER_BANK_3_PADDR,
	  .virt = REGISTER_BANK_3_VADDR,
	  .size = REGISTER_BANK_SIZE,
	  .flags = MMU_INITIAL_MAPPING_FLAG_DEVICE,
	  .name = "bank-3" },

	/* null entry to terminate the list */
	{ 0 }
};

static pmm_arena_t ram_arena = {
    .name  = "ram",
    .base  =  MEMBASE + KERNEL_LOAD_OFFSET,
    .size  =  MEMSIZE,
    .flags =  PMM_ARENA_FLAG_KMAP
};

void platform_init_mmu_mappings(void)
{
	if (plat_curr_cpu_num() == 0) {
		/* go through mmu_initial_mapping to find dynamic entry
		 * matching ram_arena (by name) and adjust it. Also update
		 * _mem_size and _mem_phys_base variables
		 */
		struct mmu_initial_mapping *m = mmu_initial_mappings;
		for (uint i = 0; i < countof(mmu_initial_mappings); i++, m++) {
			if (!(m->flags & MMU_INITIAL_MAPPING_FLAG_DYNAMIC))
				continue;

			if (strcmp(m->name, ram_arena.name) == 0) {
				/* update ram_arena */
				ram_arena.base = m->phys;
				ram_arena.size = m->size;
				ram_arena.flags = PMM_ARENA_FLAG_KMAP;

				/* update _mem_size and _mem_phys_base */
				_mem_size = m->size;
				_mem_phys_base = m->phys;
				break;
			}
		}
		pmm_add_arena(&ram_arena);
	}
}

#if WITH_SMP

static void platform_secondary_init(uint level)
{
	dprintf(SPEW, "%s: cpu_id 0x%x\n", __func__, plat_curr_cpu_num());
}

LK_INIT_HOOK_FLAGS(tegra_secondary, platform_secondary_init, LK_INIT_LEVEL_PLATFORM, LK_INIT_FLAG_SECONDARY_CPUS);

#endif
